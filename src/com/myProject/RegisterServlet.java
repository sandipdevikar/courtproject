package com.myProject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String benchno = req.getParameter("benchno");
		String courtname = req.getParameter("courtname");
		String judgename = req.getParameter("judgename");
		String caseno = req.getParameter("caseno");
		String nameofcomplainent = req.getParameter("nameofcomplainent");
		String advocateofcomplainent = req.getParameter("advocateofcomplainent");
		String complainentmobileno = req.getParameter("complainentmobileno");
		String advocatemobileno = req.getParameter("advocatemobileno");
		System.out.println(benchno);
		System.out.println(courtname);
		System.out.println(judgename);
		System.out.println(caseno);
		System.out.println(nameofcomplainent);
		System.out.println(advocateofcomplainent);
		System.out.println(complainentmobileno);
		System.out.println(advocatemobileno);
		
		DbHelper db= new DbHelper();
		String result = db.register(benchno,courtname,judgename,caseno,nameofcomplainent,advocateofcomplainent,complainentmobileno,advocatemobileno);
		if(result.equalsIgnoreCase("Success")) {
			resp.sendRedirect("Register.jsp?result=success");
		}
		else {
			resp.sendRedirect("register.jsp?result=error");
		}
		
	}
	

}
