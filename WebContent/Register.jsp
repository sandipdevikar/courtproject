<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<title>Regiter</title>
</head>
<body>
<div class="container row col-4 col-md-4 offset-md-4 offset-4">
	
<%
			if(request.getParameter("result")!=null){
				
				if(request.getParameter("result").toString().equals("success")){
					
					%>
					<label class="alert alert-success mx-5">Successfully Registered</label>
					
					<%
					
				}
				
			}
	
	%>
	</div>
	<div class="container row col-4 col-md-4 offset-md-4 offset-4">
	
	<div class="card border-light" style="border:0">
	<div class="card-title text-center">
	
	
	<label style="font-size: 34px;
    font-weight: 500;
    /* text-align: center; */
    position: relative;
    left: 97%; display:block;
">	Register</label>
	</div>
	
	
	</div>
	</div>
	</body>
	
	<div class="container row  col-4 offset-md-4 offset-4">
	<form class="form" style="width:100%" action="register" method="post">
  <div class="form-group">
   <label>Bench No</label>
    <input type="text" class="form-control" name="benchno" placeholder="">
  </div>
  <div class="form-group">
   <label>Court Name</label>
    <input type="text" class="form-control" name="courtname" placeholder="">
  </div>
  <div class="form-group">
   <label>Judge Name</label>
    <input type="text" class="form-control" name="judgename"  placeholder="">
  </div>
  <div class="form-group">
 <label>Case No</label>
    <input type="number" class="form-control" name="caseno" id="" placeholder="">
  </div>
  <div class="form-group">
    <label>Name of Complainent</label>
    <input type="text" class="form-control" id="" name="nameofcomplainent" placeholder="">
  </div>
  <div class="form-group">
    <label>Advocate of Complainent</label>
    <input type="text" class="form-control" id="" name="advocateofcomplainent" placeholder="">
  </div>
  <div class="form-group">
    <label>Complainent Mobile No</label>
    <input type="number" class="form-control" id="" name="complainentmobileno" placeholder="">
  </div>
  <div class="form-group">
    <label>Advocate Mobile No</label>
    <input type="number" class="form-control" id="" name="advocatemobileno" placeholder="">
  </div>
  
  <div class="form-group">
  
    <input type="submit" value="Register" class="btn btn-primary">
  </div>
  
  
 
  
</form>
	
	</div>
</html>